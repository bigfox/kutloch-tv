var button = opera.contexts.toolbar.createItem({
	title: 'Kutloch TV',
	icon: 'img/button-icon.png',
	disabled: false,
	popup: {
		href: 'player.html',
		width: 600,
		height: 450
	}
});

opera.contexts.toolbar.addItem(button);

var options = widget.preferences.getItem('options');
options = options === '' ? {} : JSON.parse(options);

function vlc(params, success, error) {
	var host = options.vlc.host || '';
	var port = options.vlc.port || '';

	if (host !== '') {
		var url = 'http://'+ host + (port === '' ? '' : ':' + port) +'/requests/'+ (params.uri || params.dir ? 'browse.xml' : 'status.xml');
		load(url, params, success, error);
	}
}

function shutdown(params, success, error) {
	var host = options.shutdown.host || '';
	var port = options.shutdown.port || '';
	var username = options.shutdown.username || '';
	var password = options.shutdown.password || '';

	if (host !== '') {
		var url = 'http://'+ host + (port === '' ? '' : ':' + port) +'/execute';
		load(url, params, success, error, username, password);
	}
}

function load(url, params, success, error, username, password) {
	success = typeof success === 'function' ? success : function(data, textStatus, jqXHR) {
		// console.log(textStatus);
	};

	error = typeof error === 'function' ? error : function(jqXhr, textStatus, errorThrown) {
		opera.postError(errorThrown);
	};

	username = typeof username == 'string' ? username : '';
	password = typeof password == 'string' ? password : '';

	var settings = {
		type: 'get',
		url: url,
		data: params,
		success: success,
		error: error
	};

	if (username !== '') {
		$.extend(settings, {
			username: username
		});
		if (password !== '') $.extend(settings, {
			password: password
		});
	}

	$.support.cors = true;
	$.ajax(settings);
}

function openTab(url) {
	opera.extension.tabs.create({
		url: url,
		focused: true
	});
}