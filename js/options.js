function OptionsCtrl($scope) {
	var defaultOptions = {
		vlc: {
			host: '10.10.10.20',
			port: '8080'
		},
		shutdown: {
			host: '10.10.10.20',
			port: '8090',
			username: 'admin',
			password: '123456'
		},
		places: [],
		channels: [{
			title: 'ČT1',
			uri: 'http://10.10.10.1:8080/ct1'
		}, {
			title: 'ČT2',
			uri: 'http://10.10.10.1:8080/ct2'
		}, {
			title: 'ČT24',
			uri: 'http://10.10.10.1:8080/ct24'
		}, {
			title: 'ČT4',
			uri: 'http://10.10.10.1:8080/ctsport'
		}, {
			title: 'Nova',
			uri: 'http://10.10.10.1:8081/nova'
		}, {
			title: 'Nova Cinema',
			uri: 'http://10.10.10.1:8081/novacin'
		}, {
			title: 'Prima Family',
			uri: 'http://10.10.10.1:8081/prima2'
		}, {
			title: 'Prima COOL',
			uri: 'http://10.10.10.1:8081/prima'
		}, {
			title: 'Barrandov TV',
			uri: 'http://10.10.10.1:8081/barand'
		}]
	};

	var options = widget.preferences.getItem('options');
	$scope.options = options === "" ? defaultOptions : JSON.parse(options);

	$scope.openPlayer = function() {
		opera.extension.bgProcess.openTab('player.html');
	};

	$scope.addPlace = function() {
		$scope.options.places.push({});
	};

	$scope.removePlace = function(index) {
		$scope.options.places.splice(index, 1);
	};

	$scope.addChannel = function() {
		$scope.options.channels.push({});
	};

	$scope.removeChannel = function(index) {
		$scope.options.channels.splice(index, 1);
	};

	$scope.$watch('options', function(options) {
		widget.preferences.setItem('options', JSON.stringify(options));
	}, true);
}