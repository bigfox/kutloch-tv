angular.module('filters', []).filter('volume', function() {
	return function(vol) {
		return Math.floor(vol / 2.56) + '%';
	};
});

var app = angular.module('kutloch-tv', ['filters']);

app.factory('options', function() {
	var json = widget.preferences.getItem('options');
	var options = (json === '' ? {} : JSON.parse(json));

	var save = function() {
			widget.preferences.setItem('options', JSON.stringify(options));
		};

	return {
		get places() {
			return angular.isArray(options.places) ? options.places : [];
		}, set places(places) {
			angular.extend(options, {
				places: places
			});
			save();
		}, get channels() {
			return angular.isArray(options.channels) ? options.channels : [];
		}
	};
});

app.factory('player', function() {
	var FILE_TYPES = {
		video: ['avi'],
		audio: ['mp3'],
		subtitle: ['sub', 'srt']
	};

	var createItem = function(node) {
			var item = {
				title: node.attributes['name'].value,
				uri: '',
				type: node.attributes['type'].value.replace('directory', 'dir')
			};

			if (node.hasAttribute('uri')) {
				// New version VLC
				item.uri = node.attributes['uri'].value;
			} else {
				// Old version VLC
				item.uri = node.attributes['path'].value.replace(/\\/g, '/');
			}

			if (item.type === 'file') {
				var ext = item.title.substr(item.title.lastIndexOf('.') + 1).toLowerCase();
				for (var t in FILE_TYPES) {
					if ($.inArray(ext, FILE_TYPES[t]) != -1) {
						item.type = t;
					}
				}

			} else if (item.type === 'dir') {
				if (item.title === '..') {
					item.type = 'parent';
				}

				item.title = '['+ item.title +']';
			}

			return item;
		};

	return {
		play: function(uri) {
			opera.extension.bgProcess.vlc(
			uri ? {
				command: 'in_play',
				input: uri
			} : {
				command: 'pl_play'
			});
		},
		pause: function() {
			opera.extension.bgProcess.vlc({
				command: 'pl_pause'
			});
		},
		stop: function() {
			opera.extension.bgProcess.vlc({
				command: 'pl_stop'
			});
		},
		fullscreen: function() {
			opera.extension.bgProcess.vlc({
				command: 'fullscreen'
			});
		},
		volume: function(vol) {
			opera.extension.bgProcess.vlc({
				command: 'volume',
				val: vol
			});
		},
		open: function(uri, callback) {
			var params = {};
			uri = (uri === '' ? 'file:///' : uri);

			if (uri.substr(0, 8) === 'file:///') {
				// New version VLC
				params.uri = uri;
			} else {
				// Old version VLC
				params.dir = uri;
			}

			opera.extension.bgProcess.vlc(params, function(data) {
				var nodes = data.getElementsByTagName('element');
				var items = [];

				for (var i = 0; i < nodes.length; ++i) {
					var node = nodes[i];
					items.push(createItem(node));
				}

				callback(items);
			});
		},
		shutdown: function() {
			opera.extension.bgProcess.shutdown({
				action: 'Shutdown',
				force: '1'
			}, function() {
				var scope = angular.element(document.getElementById('main')).scope();
				scope.$apply(function() {
					scope.loaded = false;
				});
			});
		}
	};
});

function PlayerCtrl($scope, options, player) {
	$scope.loaded = false;
	$scope.confirmShutdown = false;
	$scope.mode = 'tv';
	$scope.items = [];
	$scope.status = {
		state: '',
		uri: '',
		dir: '',
		volume: 256,
		volumePrev: 256,
		title: function() {
			// Find active URI in channels
			channel = options.channels.filter(function(channel) {
				return $scope.status.uri === channel.uri;
			});

			return channel.length ? channel[0].title : $scope.status.uri;
		}
	};

	$scope.shutdown = function() {
		player.shutdown();
	};

	$scope.play = function(item) {
		player.play(item ? item.uri : undefined);
	};

	$scope.pause = function() {
		player.pause($scope.status.uri);
	};

	$scope.stop = function() {
		player.stop();
	};

	$scope.fullscreen = function() {
		player.fullscreen();
	};

	$scope.volume = function(vol) {
		var volume = $scope.status.volume;

		if (typeof vol == 'undefined') {
			// Mute/unMute
			if (volume === 0) {
				volume = $scope.status.volumePrev;
			} else {
				$scope.status.volumePrev = volume;
				volume = 0;
			}

		} else {
			// Volume down/up
			volume += parseInt(vol, 10);
		}

		player.volume(Math.max(0, Math.min(volume, 512)));
	};

	$scope.open = function(item) {
		if ($scope.mode === 'places') {
			$scope.status.dir = item.uri;
			$scope.mode = 'files';

		} else if (item.type === 'channel' || item.type === 'video') {
			// Play TV channel or video file
			$scope.play(item);

		} else if (item.type === 'root' || item.type === 'parent' || item.type === 'dir') {
			// Open folder
			$scope.status.dir = item.uri;
			player.open(item.uri, UpdateBrowser);
		}
	};

	$scope.getClass = function(item) {
		var classes = [];
		var types = {
			root: 'icon-home',
			parent: 'icon-arrow-up',
			dir: 'icon-folder-close',
			channel: 'icon-play-circle',
			video: 'icon-film',
			audio: 'icon-music',
			subtitle: 'icon-font'
		};

		classes.push(types[item.type] || 'icon-file');

		if ($scope.isActive(item)) {
			classes.push('icon-white');
		}

		return classes.join(' ');
	};

	$scope.isActive = function(item) {
		// Check if item is playing
		return $scope.status.uri === item.uri;
	};

	$scope.isPlace = function(item) {
		// Check if item is in places
		return options.places.some(function(place) {
			if (typeof place.uri != 'undefined') {
				return place.uri === item.uri;
			}
		});
	};

	$scope.updatePlaces = function(item) {
		var places = angular.copy(options.places);

		if ($scope.isPlace(item)) {
			// Remove item from places
			places = places.filter(function(place) {
				return item.uri !== place.uri;
			});

		} else {
			// Add item to places
			places.push({
				title: item.title,
				uri: item.uri
			});

		}

		options.places = places;
	};

	$scope.$watch('mode', function(mode) {

		if (mode == 'tv') {
			// Load TV channels
			$scope.status.dir = '';
			$scope.items = options.channels.map(function(item) {
				item.type = 'channel';
				return item;
			});

		} else if (mode == 'places') {
			// Load places
			$scope.status.dir = '';
			$scope.items = options.places.map(function(item) {
				item.type = 'dir';
				return item;
			});

		} else if (mode == 'files') {
			// Load files
			player.open($scope.status.dir, UpdateBrowser);
		}
	});

	$scope.$watch('items', function() {
		// Add 'link to home' as first item
		if ($scope.mode === 'files') {
			$scope.items.unshift({
				title: 'Home',
				uri: 'file:///',
				type: 'root'
			});
		}
	});
}

function UpdateBrowser(items) {
	var scope = angular.element(document.getElementById('main')).scope();
	scope.$apply(function() {
		scope.items = items;
	});
}

function UpdateStatus() {
	opera.extension.bgProcess.vlc({}, function(data) {
		var obj = {},
			el;

		// Player volume
		el = data.getElementsByTagName('volume');
		if (el.length > 0) {
			obj.volume = parseInt(el[0].firstChild.data, 10);
		}

		// Player status
		el = data.getElementsByTagName('state');
		if (el.length > 0) {
			obj.state = el[0].firstChild.data;
		}

		// Now playing URI
		el = $('[name="filename"]', data);
		if (el.length > 0) {
			obj.uri = el.text();
		} else {
			el = $('information meta-information title', data);
			if (el.length > 0) {
				obj.uri = el.text();
			}
		}

		// Title
		// TODO

		var scope = angular.element(document.getElementById('main')).scope();
		scope.$apply(function() {
			scope.loaded = true;
			scope.status = angular.extend(scope.status, obj);
		});
	});
}

document.addEventListener("DOMContentLoaded", function() {
	window.setInterval(UpdateStatus, 500);
}, false);